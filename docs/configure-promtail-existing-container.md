```sh
lxc exec <container name> -- bash
apt update && apt install --yes git jq unzip ansible
ansible-galaxy install andreswebs.promtail
git clone https://gitlab.com/andreswebs/packer-lxd.git
cd packer-lxd
ansible-playbook ansible/workload.playbook.yml
```
