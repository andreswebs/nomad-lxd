Expose Grafana port from container to host:

```sh
lxc config device add monitor grafana proxy listen=tcp:0.0.0.0:3000 connect=tcp:127.0.0.1:3000
```
