# packer-lxd

Packer templates for base LXD containers with monitoring dependencies installed.

## Architecture

![Basic LXD monitoring](docs/diagrams/lxd-monitoring.drawio.png "Basic LXD monitoring")

| Legend: |
| --- |
| whitespace represents a virtual server; |
| boxes represent LXD containers; |
| elipses represent host processes; |
| words between `<>` represent processes in LXD containers or host; |

## Templates

- [base](packer/base.pkr.hcl): base dependencies
- [monitor](packer/monitor.pkr.hcl): container with Prometheus, Grafana, Loki, Alertmanager
- [workload](packer/workload.pkr.hcl): container with Promtail

## Run

```sh
./scripts/packer.install.bash
./scripts/packer.validate.bash
./scripts/packer.build.bash
```

## Authors

**Andre Silva** - [@andreswebs](https://github.com/andreswebs)

## License

This project is licensed under the [Unlicense](UNLICENSE.md).
