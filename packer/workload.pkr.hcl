locals {
  base_image_name            = "local:base-ubuntu-20.04"
  image_name                 = "workload-ubuntu-20.04"
  ansible_role_promtail      = "andreswebs.promtail"
}

source "lxd" "workload" {

  image        = local.base_image_name
  output_image = local.image_name

  publish_properties = {
    description = "Workload image from ${local.base_image_name}"
  }

}

locals {
  playbook_dir = "${path.root}/../ansible"
}

build {
  sources = ["source.lxd.workload"]

  provisioner "shell" {
    inline = [
      "ansible-galaxy install ${local.ansible_role_promtail}",
    ]
  }

  provisioner "ansible-local" {
    clean_staging_directory = true
    command                 = "ANSIBLE_LOCALHOST_WARNING=false ansible-playbook"
    playbook_dir            = local.playbook_dir
    playbook_file           = "${local.playbook_dir}/workload.playbook.yml"
  }

}
